from django.shortcuts import render, get_object_or_404
from django.views import generic
from video.models import Video


class GlobalMixin(object):
    def get_context_data(self, *args, **kwargs):
        context = super(GlobalMixin, self).get_context_data(*args, **kwargs)
        context['video_list'] = Video.objects.all().order_by('pk')
        return context


class IndexView(GlobalMixin, generic.ListView):
    template_name = 'index.html'
    model = Video
    context_object_name = 'videos'


class VideoView(GlobalMixin, generic.DetailView):
    template_name = 'video.html'
    model = Video

    def get_context_data(self, *args, **kwargs):
        context = super(VideoView, self).get_context_data(*args, **kwargs)
        context['hello'] = "Hello World"
        context['video_list'] = Video.objects.all().order_by('-pk')
        return context
