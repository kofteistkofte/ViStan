from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=200, verbose_name="Başlık")
    desc = models.TextField(verbose_name="Açıklama")

    class Meta:
        verbose_name = "Kategori"
        verbose_name_plural = "Kategoriler"

    def __str__(self):
        return self.title


class Video(models.Model):
    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE, verbose_name="Kategori")
    title = models.CharField(max_length=200, verbose_name="Başlık")
    desc = models.TextField(verbose_name="Açıklama")
    file = models.FileField(verbose_name="Video Konumu", upload_to="videos/%Y/%m")

    class Meta:
        verbose_name = "Video"
        verbose_name_plural = "Videolar"

    def __str__(self):
        return self.title
