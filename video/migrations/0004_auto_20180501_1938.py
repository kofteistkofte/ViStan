# Generated by Django 2.0.4 on 2018-05-01 19:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('video', '0003_video_category'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='file',
            field=models.FileField(upload_to='videos/%Y/%m/%d', verbose_name='Video Konumu'),
        ),
    ]
