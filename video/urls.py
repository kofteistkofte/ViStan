from django.urls import path
from video import views

app_name = "videos"

urlpatterns = [
    path('', views.IndexView.as_view(), name="index"),
    path('<int:pk>/', views.VideoView.as_view(), name="video"),
]
