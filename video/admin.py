from django.contrib import admin
from video.models import Video, Category


class Mahmut(admin.ModelAdmin):
    search_fields = ('title',)
    list_display = ('title', 'category')
    list_editable = ('category',)
    fields = (('title', 'category'), 'desc', 'file')


admin.site.register(Category)
admin.site.register(Video, Mahmut)
